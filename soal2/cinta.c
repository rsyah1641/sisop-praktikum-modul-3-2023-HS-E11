#include <stdio.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define NUM_THREADS 20

int (*value)[5];
unsigned long long result[4][5];

// Fungsi untuk menghitung faktorial
unsigned long long factorial(int n) {
    if(n == 0) {
        return 1;
    }
    return n * factorial(n - 1);
}

// Fungsi yang dijalankan oleh thread untuk menghitung faktorial dari elemen matriks
void *calculate_factorial(void *arg) {
    int index = (int)arg;
    int row = index / 5;
    int col = index % 5;
    result[row][col] = factorial(value[row][col]);
}

int main() {
    key_t key = 1641;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    value = shmat(shmid, NULL, 0);
    
    // Mencetak matriks hasil perkalian
    printf("Matriks Hasil:\n");
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 5; j++) {
            printf("%d ", value[i][j]);
        }
        printf("\n");
    }
    
    pthread_t threads[NUM_THREADS];
    
    // Membuat thread untuk menghitung faktorial dari setiap elemen matriks
    for(int i = 0; i < NUM_THREADS; i++) {
        pthread_create(&threads[i], NULL, calculate_factorial, (void *)i);
    }
    
    // Menunggu semua thread selesai
    for(int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    
    // Mencetak matriks hasil faktorial
    printf("\nMatriks Faktorial:\n");
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 5; j++) {
            printf("%llu ", result[i][j]);
        }
        printf("\n");
    }
    
    // Melepaskan memori bersama
    shmdt(value);
    shmctl(shmid, IPC_RMID, NULL);
    
    return 0;
}
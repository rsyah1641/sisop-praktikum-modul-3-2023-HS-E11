#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define ROWS_A 4
#define COLS_A 2
#define ROWS_B 2
#define COLS_B 5

void print_matrix(int rows, int cols, int matrix[rows][cols]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void multiply_matrices(int rows_a, int cols_a, int a[rows_a][cols_a], int rows_b, int cols_b, int b[rows_b][cols_b], int c[rows_a][cols_b]) {
    for (int i = 0; i < rows_a; i++) {
        for (int j = 0; j < cols_b; j++) {
            c[i][j] = 0;
            for (int k = 0; k < cols_a; k++) {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
}

int main() {
    int a[ROWS_A][COLS_A], b[ROWS_B][COLS_B], c[ROWS_A][COLS_B];
    srand(time(0));
    
    // Mengisi matriks A dengan bilangan acak
    for (int i = 0; i < ROWS_A; i++) {
        for (int j = 0; j < COLS_A; j++) {
            a[i][j] = rand() % 5 + 1;
        }
    }
    
    // Mengisi matriks B dengan bilangan acak
    for (int i = 0; i < ROWS_B; i++) {
        for (int j = 0; j < COLS_B; j++) {
            b[i][j] = rand() % 4 + 1;
        }
    }
    
    // Mencetak matriks A
    printf("Matriks A:\n");
    print_matrix(ROWS_A, COLS_A, a);
    
    // Mencetak matriks B
    printf("\nMatriks B:\n");
    print_matrix(ROWS_B, COLS_B, b);
    
    // Menghitung perkalian matriks A dan B
    multiply_matrices(ROWS_A, COLS_A, a, ROWS_B, COLS_B, b, c);
    
    // Menyimpan hasil perkalian matriks A dan B ke dalam memori bersama
    key_t key = 1641;
    int (*value)[COLS_B];
    int shmid = shmget(key, sizeof(int[ROWS_A][COLS_B]), IPC_CREAT | 0666);
    value = shmat(shmid, NULL, 0);
    
    // Mencetak hasil perkalian matriks A dan B
    printf("\nMatriks AXB:\n");
    for (int i = 0; i < ROWS_A; i++) {
        for (int j = 0; j < COLS_B; j++) {
            value[i][j] = c[i][j];
             printf("%d ", value[i][j]);
        }
        printf("\n");
    }
    
    sleep(5);
    // Melepaskan memori bersama
    shmdt(value);
    
    return 0;
}
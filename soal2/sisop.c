#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define ROWS 4
#define COLS 5

// Fungsi untuk menghitung faktorial
unsigned long long factorial(int n) {
    if(n == 0) {
        return 1;
    }
    return n * factorial(n - 1);
}

void print_matrix(int rows, int cols, int matrix[rows][cols]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void print_factorial_matrix(int rows, int cols, int matrix[rows][cols]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%llu ", factorial(matrix[i][j]));
        }
        printf("\n");
    }
}

int main() {
    key_t key = 1641;
    int (*value)[COLS];
    int shmid = shmget(key, sizeof(int[ROWS][COLS]), IPC_CREAT | 0666);
    value = shmat(shmid, NULL, 0);
    
    // Mencetak matriks hasil perkalian
    printf("Matriks Hasil:\n");
    print_matrix(ROWS, COLS, value);
    
    // Mencetak matriks hasil faktorial
    printf("\nMatriks Faktorial:\n");
    print_factorial_matrix(ROWS, COLS, value);
    
    // Melepaskan memori bersama
    shmdt(value);
    shmctl(shmid, IPC_RMID, NULL);
    
    return 0;
}
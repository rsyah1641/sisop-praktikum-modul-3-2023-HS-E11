#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>

#define FILE_URL "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download"
#define FILE_NAME "hehe.zip"

void download_file(const char *file_url, const char *file_name) {
    pid_t child_id = fork();
    if (child_id == 0) {
        execlp("wget", "wget", "-O", file_name, file_url, NULL);
    }
    int status;
    while ((wait(&status)) > 0);
}

void unzip_file(const char *file_name) {
    pid_t child_id = fork();
    if (child_id == 0) {
        execlp("unzip", "unzip", file_name, NULL);
    }
    int status;
    while ((wait(&status)) > 0);
}

void remove_file(const char *file_name) {
    pid_t child_id = fork();
    if (child_id == 0) {
        execlp("rm", "rm", file_name, NULL);
    }
    int status;
    while ((wait(&status)) > 0);
}

int main() {
    download_file(FILE_URL, FILE_NAME);
    unzip_file(FILE_NAME);
    remove_file(FILE_NAME);

    printf("Dowload Complete\n");
}

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <time.h>

// dibuat fungsi untuk melakukan dowload file dari drive yang diberikan
void drive_dowload() {
    pid_t child_pid = fork();
    if (child_pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        char *argv[] = {"wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        if (execvp("wget", argv) == -1) {
            perror("execvp");
            exit(EXIT_FAILURE);
        }
    } else { // parent process
        waitpid(child_pid, NULL, 0);
        printf("File di google drive berhasil di dowload!\n");
    }
}

// dilakukan untuk menextract file zip yang sudah didownload dari gdrive sebelumnya.
void grive_unzip() {
    pid_t child_pid = fork();
    if (child_pid == -1) {
        perror("Proses Fork Gagal");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        execl("/usr/bin/unzip", "unzip", "binatang.zip", NULL);
        perror("Proses execl gagal");
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
        printf("File berhasil di unzip\n");
    }
}

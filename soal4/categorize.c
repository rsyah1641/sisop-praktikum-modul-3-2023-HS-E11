#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <ctype.h>
#include <unistd.h>

#define MAX_LINE_LENGTH 300

int extCount = 0;
char ekstension[MAX_LINE_LENGTH][10];
int ekstensionDir[MAX_LINE_LENGTH];
char buffer[MAX_LINE_LENGTH];
int max = 0;
pthread_t threads[2];

int mode = 1;
char param1[MAX_LINE_LENGTH];
char param2[MAX_LINE_LENGTH];
char param3[MAX_LINE_LENGTH];

// Fungsi untuk logging
void *logging() {
    char file_log[] = "log.txt";
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    char write[MAX_LINE_LENGTH];
    if (mode == 1) {
        sprintf(write, "%s AKSES %s", datetime, param1);
    } else if (mode == 2) {
        sprintf(write, "%s PINDAH %s file : %s > %s", datetime, param1, param2, param3);
    } else {
        sprintf(write, "%s BUAT %s", datetime, param1);
    }
    char cmd[MAX_LINE_LENGTH];
    sprintf(cmd, "echo '%s' >> %s", write, file_log);
    system(cmd);
}

// Membuat thread untuk logging,
void create_thread_logging(int modeNew, char param1New[], char param2New[], char param3New[]) {
    mode = modeNew;
    strcpy(param1, param1New);
    strcpy(param2, param2New);
    strcpy(param3, param3New);
    pthread_join(threads[1], NULL);
    pthread_create(&threads[1], NULL, logging, NULL);
    pthread_join(threads[1], NULL);
}

// Mendapatkan ekstensi dari nama file
const char *get_filename_ext(char *filename) {
    const char *dot = strrchr(filename, '.');
    if (!dot || dot == filename) return "";
    return dot + 1;
}

// Mengecek ekstensi
int check_ekstension(char eks[]) {
    for (int i = 0; i < extCount; i++) {
        if (strcmp(eks, ekstension[i]) == 0)
            return i;
    }
    return -1;
}

// Membuat direktori
void *buatdir() {
    char folder_name[MAX_LINE_LENGTH];
    strcpy(folder_name, "./categorized");
    char cmd[MAX_LINE_LENGTH];
    sprintf(cmd, "mkdir -p %s", folder_name);
    system(cmd);
    create_thread_logging(3, folder_name, "null", "null");
    FILE *textfile;
    textfile = fopen("extensions.txt", "r");
    int index = 0;
    char line[MAX_LINE_LENGTH];

    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        line[strlen(line) - 2] = '\0';
        sprintf(cmd, "mkdir -p %s/%s", folder_name, line);
        strcpy(ekstension[index], line);
        ekstensionDir[index] = 0;
        index++;
        extCount++;
        system(cmd);

        sprintf(buffer, "%s/%s", folder_name, line);
        create_thread_logging(3, buffer, "null", "null");
        fclose(textfile);

    sprintf(cmd, "mkdir -p %s/other", folder_name);
    system(cmd);

    sprintf(buffer, "%s/other", folder_name);
    create_thread_logging(3, buffer, "null", "null");
}

// Mendaftar direktori
void listdir(const char *name) {
    DIR *dir;
    struct dirent *entry;
    char cmd[MAX_LINE_LENGTH];

    // Membuka direktori
    if (!(dir = opendir(name)))
        return;

    // Membaca isi direktori
    while ((entry = readdir(dir)) != NULL) {
        // Jika entry adalah direktori
        if (entry->d_type == DT_DIR) {
            char path[1024];
            // Lewati direktori "." dan ".."
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            create_thread_logging(1, path, "null", "null");
            listdir(path);
        } else {
            // Jika entry adalah file
            sprintf(cmd, "echo '%s/%s' >> listFiles.txt", name, entry->d_name);
            system(cmd);
        }
    }
    closedir(dir);
}

// Mendaftar file
void *listfile() {
    char charMax[3];
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "max.txt");
    textfile = fopen(nama_file, "r");
    fgets(charMax, 3, textfile);
    fclose(textfile);
    max = atoi(charMax);

    listdir("./files");
}

// Memindahkan file
void *pindahfile() {
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "listFiles.txt");
    textfile = fopen(nama_file, "r");

    // Membaca isi file listFiles.txt
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        line[strlen(line) - 1] = '\0';
        char eks[MAX_LINE_LENGTH];
        sprintf(eks,"%s",get_filename_ext(line));

        // Mengubah ekstensi menjadi huruf kecil
        for (int i = 0; eks[i]; i++) {
            eks[i] = tolower(eks[i]);
        }

        // Jika ekstensi tidak ditemukan di daftar ekstensi
        if (check_ekstension(eks) == -1) {
            sprintf(cmd, "cp '%s' ./categorized/other", line);
            system(cmd);
            create_thread_logging(2, "other", line,"./categorized/other" );
        } else {
            // Jika jumlah file dengan ekstensi ini kurang dari max
            if (ekstensionDir[check_ekstension(eks)] < max) {
                sprintf(cmd, "cp '%s' ./categorized/%s", line, eks);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer, "./categorized/%s", eks);
                create_thread_logging(2, eks,line , buffer );
            } else {
                int num = ekstensionDir[check_ekstension(eks)] / max + 1;
                sprintf(buffer, "./categorized/%s %d", eks,num);
                if (access(buffer,F_OK) != 0) {
                    create_thread_logging(3, buffer,"null","null" );
                }
                sprintf(cmd,"mkdir -p './categorized/%s %d'",eks,num);
                system(cmd);

                sprintf(cmd,"cp '%s' './categorized/%s %d'",line ,eks,num);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer,"./categorized/%s %d",eks,num);
                create_thread_logging(2 ,eks,line ,buffer );
            }
        }
        }
    fclose(textfile);
}

// Menghitung jumlah ekstensi
void *hitung_eks() {
    strcpy(ekstension[extCount], "other");

    DIR *directory;
    struct dirent *entry;

    directory = opendir("./categorized/other");

    create_thread_logging(1,"./categorized/other","null","null");

    while ((entry = readdir(directory)) != NULL) {
        if (entry->d_type == DT_REG) {
            ekstensionDir[extCount]++;
        }
    }
}

int main() {
    pthread_create(&threads[0], NULL, listfile, NULL);
    pthread_join(threads[0], NULL);

    pthread_create(&threads[0], NULL, buatdir, NULL);
    pthread_join(threads[0], NULL);

    pthread_create(&threads[0], NULL, pindahfile, NULL);
    pthread_join(threads[0], NULL);

    pthread_create(&threads[0], NULL, hitung_eks, NULL);
    pthread_join(threads[0], NULL);

    remove("listFiles.txt");
    rmdir("files");

    return 0;
}

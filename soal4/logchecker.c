#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 300

typedef struct {
    char name[MAX_LINE_LENGTH];
    int count;
} Dir;

Dir dir[MAX_LINE_LENGTH];
int indexDir = 0;

typedef struct {
    char name[MAX_LINE_LENGTH];
    int count;
} Eks;

Eks eks[MAX_LINE_LENGTH];
int indexEks = 0;

// Menghitung jumlah akses. 
int hitung_akses() {
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    int counter = 0;
    textfile = fopen("log.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        if (strstr(line, "ACCESSED") != NULL) {
            counter++;
        }
    }
    fclose(textfile);
    return counter;
}

// Mengecek direktori
int cek_dir(char dirs[]) {
    for (int i = 0; i < indexDir; i++) {
        if (strcmp(dirs, dir[i].name) == 0)
            return i;
    }
    return -1;
}

// Mengecek ekstensi
int cek_eks(char ekss[]) {
    for (int i = 0; i < indexEks; i++) {
        if (strcmp(ekss, eks[i].name) == 0)
            return i;
    }
    return -1;
}

// Menghitung semua direktori
void hitung_semua_dir() {
    FILE *textfile;
    char line[MAX_LINE_LENGTH];

    textfile = fopen("log.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        if (strstr(line, "MADE ") != NULL) {
            line[strlen(line) - 1] = '\0';
            char *pos = strstr(line, "MADE ");
            char *file = pos + strlen("MADE ");

            strcpy(dir[indexDir].name, file);
            dir[indexDir].count = 0;
            indexDir++;
        }
    }
    fclose(textfile);

    textfile = fopen("log.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        if (strstr(line, "> ") != NULL) {
            line[strlen(line) - 1] = '\0';
            char *pos = strstr(line, "> ");
            char *file = pos + strlen("> ");

            int num = cek_dir(file);
            dir[num].count++;
        }
    }
    fclose(textfile);

    for (int i = 0; i < indexDir; i++) {
        for (int j = 0; j < indexDir - 1; j++) {
            if (dir[j].count > dir[j + 1].count) {
                Dir temp;
                temp = dir[j];
                dir[j] = dir[j + 1];
                dir[j + 1] = temp;
            }
        }
    }

    for (int i = 0; i < indexDir; i++) {
        printf("%s : %d\n", dir[i].name, dir[i].count);
    }
}

// Menghitung semua ekstensi
void hitung_semua_eks() {
    FILE *textfile;
    char line[MAX_LINE_LENGTH];

    textfile = fopen("extensions.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        line[strlen(line) - 2] = '\0';
        strcpy(eks[indexEks].name, line);
        eks[indexEks].count = 0;
        indexEks++;
    }
    fclose(textfile);
    strcpy(eks[indexEks].name, "other");
    eks[indexEks].count = 0;
    indexEks++;
textfile = fopen("log.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        if (strstr(line, "MOVED ") != NULL) {
            line[strlen(line) - 1] = '\0';
            char *start = strstr(line, "MOVED ");
            char *en = strstr(start, " file :");
            char file[MAX_LINE_LENGTH];
            strncpy(file, start + strlen("MOVED "), en - start - strlen("MOVED "));
            file[en - start - strlen("MOVED ")] = '\0';

            int num = cek_eks(file);
            eks[num].count++;
        }
    }
    fclose(textfile);

    for (int i = 0; i < indexEks; i++) {
        for (int j = 0; j < indexEks - 1; j++) {
            if (eks[j].count > eks[j + 1].count) {
                Eks temp;
                temp = eks[j];
                eks[j] = eks[j + 1];
                eks[j + 1] = temp;
            }
        }
    }

    for (int i = 0; i < indexEks; i++) {
        printf("%s : %d\n", eks[i].name, eks[i].count);
    }
}

int main() {
    printf(" menghitung banyaknya ACCESSED : %d\n\n", hitung_akses());

    printf(" list seluruh folder yang telah dibuat : \n");
    hitung_semua_dir();

    printf("\n banyaknya total file tiap extension, terurut secara ascending : \n");
    hitung_semua_eks();
}

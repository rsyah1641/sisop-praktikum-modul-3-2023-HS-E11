# sisop-praktikum-modul-1-2023-HS-E11



## Anggota Kelompok E11
| Nama                      | NRP        |
|---------------------------|------------|
| Muhammad Febriansyah      | 5025211164 |
| Sastiara Maulikh          | 5025201257 |
| Schaquille Devlin Aristano| 5025211211 | 

# Soal 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 


# Catatan
- Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3
- Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
Huruf A	: 01000001
Huruf a	: 01100001
- Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
- Agar lebih mudah, ubah semua huruf kecil ke huruf kapital


## Soal 1 bagian a
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

## Cara Penyelesaian
di buat suatu fungsi untuk Menghitung frekuensi karakter dalam file. Denganungsi yang membaca setiap karakter dalam file menggunakan ```fgetc``` hingga mencapai akhir file ```(EOF)```. Untuk setiap karakter yang dibaca, ia menambahkan elemen yang sesuai dalam array ```lim```

Dan Beberapa Penjelasan Di Nomor 4
## Source Code
```sh

// Menghitung frekuensi karakter dalam file
void hitung_frekuensi(char *filename)
{
    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        perror("File Cannot Open");
        exit(EXIT_FAILURE);
    }

    int no;
    while ((no = fgetc(file)) != EOF) {
        lim[no]++;
    }

    fclose(file);
}


```

# Soal 1 bagian b
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

### Cara Penyelesaian
Dilakukan penghitungan frekuensi dari file yang didowload dengan menggunakan kode sebelumnya. lalu dibuat fungsi ```bangun_pohon_huffman(...)``` guna untuk Membangun pohon Huffman dari array frekuensi dan ```bangun_tabel_kode(...)``` guna untuk Membangun tabel kode dari pohon Huffman, kemudian dilakukan ```tulis_pohon_huffman(...)``` guna untuk Menulis pohon Huffman ke file lalu ,fungsi membaca setiap karakter dalam file input dan menulis kode Huffman yang sesuai ke file output. Terakhir, dilakukan penghitungan rasio kompresi dengan membandingkan ukuran file input dan output.

### Source Code
```sh

// Membangun pohon Huffman dari array frekuensi
node_simpul *bangun_pohon_huffman(int *lim)
{
    node_simpul *nodes[256] = {NULL};
    int n_nodes = 0;

    for (int i = 0; i < 256; i++) {
        if (lim[i] > 0) {
            node_simpul *node = (node_simpul *) malloc(sizeof(node_simpul));
            node->no = (unsigned char)i;
            node->lim = lim[i];
            node->left_child = NULL;
            node->right_child = NULL;

            nodes[n_nodes++] = node;
        }
    }

    while (n_nodes > 1) {
        // Find two nodes with lowest frequency
        int min1 = 0, min2 = 1;
        if (nodes[min1]->lim > nodes[min2]->lim) {
            int tmp = min1;
            min1 = min2;
            min2 = tmp;
        }

        for (int i = 2; i < n_nodes; i++) {
            if (nodes[i]->lim < nodes[min1]->lim) {
                min2 = min1;
                min1 = i;
            } else if (nodes[i]->lim < nodes[min2]->lim) {
                min2 = i;
            }
        }

        // Create new node and add to list
        node_simpul *new_node = (node_simpul *) malloc(sizeof(node_simpul));
        new_node->no = '\0';
        new_node->lim = nodes[min1]->lim + nodes[min2]->lim;
        new_node->left_child = nodes[min1];
        new_node->right_child = nodes[min2];

        nodes[min1] = new_node;
        nodes[min2] = nodes[--n_nodes];
    }

    return nodes[0];
}

// Membangun tabel kode dari pohon Huffman
void bangun_tabel_kode(node_simpul *node, unsigned int dataa, int lost, save_hufmand **table, int *size)
{
    if (node->left_child == NULL && node->right_child == NULL) {
        // Found a leaf node, add dataa to table
        (*size)++;
        (*table) = (save_hufmand *) realloc(*table, sizeof(save_hufmand) * (*size));
        (*table)[*size - 1].no = node->no;
        (*table)[*size - 1].dataa = dataa;
        (*table)[*size - 1].lost = lost;
    } else {
        bangun_tabel_kode(node->left_child, dataa << 1, lost + 1, table, size);
        bangun_tabel_kode(node->right_child, (dataa << 1) | 1, lost + 1, table, size);
    }
}

// Menulis pohon Huffman ke file
void tulis_pohon_huffman(node_simpul *node, int output_fd)
{
    if (node->left_child == NULL && node->right_child == NULL) {
        // Found a leaf node, write '1' bit followed by 8-bit character dataa
        unsigned char byte = (1 << 7) | node->no;
        write(output_fd, &byte, 1);
    } else {
        // Write '0' bit and recursively write subtrees
        unsigned char byte = 0;
        write(output_fd, &byte, 1);

        tulis_pohon_huffman(node->left_child, output_fd);
        tulis_pohon_huffman(node->right_child, output_fd);
    }
}

// Fungsi untuk kompresi file
void kompres(char *input_filename, char *output_filename)
{
    hitung_frekuensi(input_filename);
    node_simpul *root = bangun_pohon_huffman(lim);

    save_hufmand *table = NULL;
    int size = 0;
    bangun_tabel_kode(root, 0, 0, &table, &size);

    int output_fd = open(output_filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    if (output_fd == -1) {
        perror("Error opening output file");
        exit(EXIT_FAILURE);
    }

    tulis_pohon_huffman(root, output_fd);

    unsigned char buffer[size_bufer];
    unsigned int bit_buffer = 0;
    int num_bits = 0;

    FILE *input_file = fopen(input_filename, "r");

    if (input_file == NULL) {
        perror("Error opening input file");
        exit(EXIT_FAILURE);
    }

    int no;
    while ((no = fgetc(input_file)) != EOF) {
        for (int i = 0; i < size; i++) {
            if (table[i].no == no) {
                bit_buffer |= table[i].dataa << num_bits;
                num_bits += table[i].lost;

                while (num_bits >= 8) {
                    buffer[0] = bit_buffer & 0xff;
                    write(output_fd, buffer, 1);

                    bit_buffer >>= 8;
                    num_bits -= 8;
                }
            }
              }
    }

    // Flush remaining bits to output
    if (num_bits > 0) {
        buffer[0] = bit_buffer & 0xff;
        write(output_fd, buffer, 1);
    }

    fclose(input_file);
    close(output_fd);

    // Calculate compression ratio
    struct stat input_stat, output_stat;

    if (stat(input_filename, &input_stat) == -1 || stat(output_filename, &output_stat) == -1) {
        perror("Error getting file information");
        exit(EXIT_FAILURE);
    }

    printf("Original size: %lld bytes\n", (long long) input_stat.st_size);
    printf("Compressed size: %lld bytes\n", (long long) output_stat.st_size);
}

```


## soal 1 bagian c
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

### Cara Penyelesaian
Setiap karakter dalam file input, ubah karakter tersebut menjadi kode Huffman menggunakan tabel kode Huffman yang telah dibangun sebelumnya. Kemudian, kirim kode Huffman tersebut ke program dekompresi menggunakan pipe. Program dekompresi akan menerima kode Huffman melalui pipe dan mengubahnya kembali menjadi karakter asli menggunakan pohon Huffman yang telah disimpan di file terkompresi. Nb : proses bisa jadi terjadi saat kegiatan kompresi berlangsung
### Source Code
```sh

// Fungsi untuk kompresi file
void kompres(char *input_filename, char *output_filename)
{
    hitung_frekuensi(input_filename);
    node_simpul *root = bangun_pohon_huffman(lim);

    save_hufmand *table = NULL;
    int size = 0;
    bangun_tabel_kode(root, 0, 0, &table, &size);

    int output_fd = open(output_filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    if (output_fd == -1) {
        perror("Error opening output file");
        exit(EXIT_FAILURE);
    }

    tulis_pohon_huffman(root, output_fd);

    unsigned char buffer[size_bufer];
    unsigned int bit_buffer = 0;
    int num_bits = 0;

    FILE *input_file = fopen(input_filename, "r");

    if (input_file == NULL) {
        perror("Error opening input file");
        exit(EXIT_FAILURE);
    }

    int no;
    while ((no = fgetc(input_file)) != EOF) {
        for (int i = 0; i < size; i++) {
            if (table[i].no == no) {
                bit_buffer |= table[i].dataa << num_bits;
                num_bits += table[i].lost;

                while (num_bits >= 8) {
                    buffer[0] = bit_buffer & 0xff;
                    write(output_fd, buffer, 1);

                    bit_buffer >>= 8;
                    num_bits -= 8;
                }
            }
              }
    }

    // Flush remaining bits to output
    if (num_bits > 0) {
        buffer[0] = bit_buffer & 0xff;
        write(output_fd, buffer, 1);
    }

    fclose(input_file);
    close(output_fd);

    // Calculate compression ratio
    struct stat input_stat, output_stat;

    if (stat(input_filename, &input_stat) == -1 || stat(output_filename, &output_stat) == -1) {
        perror("Error getting file information");
        exit(EXIT_FAILURE);
    }

    printf("Original size: %lld bytes\n", (long long) input_stat.st_size);
    printf("Compressed size: %lld bytes\n", (long long) output_stat.st_size);
}


```

## Soal 1 bagian D
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

### Cara Penyelesaian
baca pohon Huffman dari file terkompresi. Kemudian, baca kode Huffman yang diterima dan dilakukan dekompresi dengan menggunakan pohon Huffman yang telah dibaca dari file terkompresi. Hasil dekompresi akan merubah kode huffman menjadi karakter asli.

### Source Kode
```sh

// Membaca pohon Huffman dari file
node_simpul *baca_pohon_huffman(int input_fd)
{
    unsigned char byte;
    ssize_t nread = read(input_fd, &byte, 1);

    if (nread == 0 || (nread == 1 && byte == 0)) {
        // Empty tree or end of subtree, return NULL
        return NULL;
    }

    if (byte & (1 << 7)) {
        // Leaf node, read 
        unsigned char dataa = byte & ~(1 << 7);
        node_simpul *node = (node_simpul *) malloc(sizeof(node_simpul));
        node->no = dataa;
        node->lim = lim[dataa];
        node->left_child = NULL;
        node->right_child = NULL;

        return node;


    } else {
        // Internal node, recursively read subtrees
        node_simpul *node = (node_simpul *) malloc(sizeof(node_simpul));
        node->no = '\0';
        node->lim = 0;
        node->left_child = baca_pohon_huffman(input_fd);
        node->right_child = baca_pohon_huffman(input_fd);

        return node;
    }
}

// Fungsi untuk dekompresi file
void dekompres(char *input_filename, char *output_filename)
{
    int input_fd = open(input_filename, O_RDONLY);

    if (input_fd == -1) {
        perror("Error opening input file");
        exit(EXIT_FAILURE);
    }

    node_simpul *root = baca_pohon_huffman(input_fd);

    FILE *output_file = fopen(output_filename, "w");

    if (output_file == NULL) {
        perror("Error opening output file");
        exit(EXIT_FAILURE);
    }

    node_simpul *node = root;
    unsigned char buffer[size_bufer];
    int buffer_pos = size_bufer;

    while (1) {
        if (buffer_pos == size_bufer) {
            ssize_t nread = read(input_fd, buffer, size_bufer);

            if (nread == 0) {
                break;  // End of input file
            }

            buffer_pos = 0;
        }

        unsigned char byte = buffer[buffer_pos++];

        for (int i = 0; i < 8; i++) {
            if (node->left_child == NULL && node->right_child == NULL) {
                // Found a leaf node, write character to output file
                fputc(node->no, output_file);
                node = root;
            }

            if (byte & (1 << (7 - i))) {
                node = node->right_child;
            } else {
                node = node->left_child;
            }
        }
          }

    fclose(output_file);
    close(input_fd);
}

```
### Hasil Output
cara run dan output akhir
![image](http://itspreneur.com/wp-content/uploads/2023/05/Screenshot-67-e1683905765439.png)


# Soal 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

#catatan
- wajib menerapkan konsep share memory
- cinta.c Wajib menerapkan thread dan multithreading dalam penghitungan faktorial
- sisop.c tanpa menggunakan thread dan multithreading

### inisiasi Variable
Menginisialisasi variabel-variabel yang akan digunakan

```sh
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

```

### Soal 2 bagian A
Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

### Cara Penyelesaian
Di buat kode untuk menghitung perkalian matriks A dan B yang disimpan kedalam array 2 dimensi. Matriks A dan B diisi dengan bilangan acak ```srand```.Kemudian hasil perkalian disimpan dalam memori bersama dengan menggunakan ```shmget``` dengan key_t yang sama dan dicetak ke layar. Setelah 10 detik, memori bersama dilepaskan.

### Source Code
```sh
 #include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define ROWS_A 4
#define COLS_A 2
#define ROWS_B 2
#define COLS_B 5

void print_matrix(int rows, int cols, int matrix[rows][cols]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void multiply_matrices(int rows_a, int cols_a, int a[rows_a][cols_a], int rows_b, int cols_b, int b[rows_b][cols_b], int c[rows_a][cols_b]) {
    for (int i = 0; i < rows_a; i++) {
        for (int j = 0; j < cols_b; j++) {
            c[i][j] = 0;
            for (int k = 0; k < cols_a; k++) {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
}

int main() {
    int a[ROWS_A][COLS_A], b[ROWS_B][COLS_B], c[ROWS_A][COLS_B];
    srand(time(0));
    
    // Mengisi matriks A dengan bilangan acak
    for (int i = 0; i < ROWS_A; i++) {
        for (int j = 0; j < COLS_A; j++) {
            a[i][j] = rand() % 5 + 1;
        }
    }
    
    // Mengisi matriks B dengan bilangan acak
    for (int i = 0; i < ROWS_B; i++) {
        for (int j = 0; j < COLS_B; j++) {
            b[i][j] = rand() % 4 + 1;
        }
    }
    
    // Mencetak matriks A
    printf("Matriks A:\n");
    print_matrix(ROWS_A, COLS_A, a);
    
    // Mencetak matriks B
    printf("\nMatriks B:\n");
    print_matrix(ROWS_B, COLS_B, b);
    
    // Menghitung perkalian matriks A dan B
    multiply_matrices(ROWS_A, COLS_A, a, ROWS_B, COLS_B, b, c);
    
    // Menyimpan hasil perkalian matriks A dan B ke dalam memori bersama
    key_t key = 1641;
    int (*value)[COLS_B];
    int shmid = shmget(key, sizeof(int[ROWS_A][COLS_B]), IPC_CREAT | 0666);
    value = shmat(shmid, NULL, 0);
    
    // Mencetak hasil perkalian matriks A dan B
    printf("\nMatriks AXB:\n");
    for (int i = 0; i < ROWS_A; i++) {
        for (int j = 0; j < COLS_B; j++) {
            value[i][j] = c[i][j];
             printf("%d ", value[i][j]);
        }
        printf("\n");
    }
    
    sleep(5);
    // Melepaskan memori bersama
    shmdt(value);
    
    return 0;
}

```

## Soal 2 bagian B & C
Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory) Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …


### Cara Penyelesaian
Progkam Ini dibuat untuk menghitung faktorial dari setiap elemen dalam matriks 4x5. Matriks disimpan dalam memori bersama dan diakses menggunakan fungsi ``shmat```. Program ini membuat 20 thread, satu untuk setiap elemen dalam matriks, dan setiap thread menghitung faktorial dari elemen yang sesuai menggunakan fungsi ```factorial```. Hasilnya disimpan dalam array ```result``` dan dicetak ke terminal. Kemudian, memori bersama dilepaskan dan dihapus menggunakan fungsi ```shmdt``` dan ```shmctl```.

### Source Code
``` sh
#include <stdio.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define NUM_THREADS 20

int (*value)[5];
unsigned long long result[4][5];

// Fungsi untuk menghitung faktorial
unsigned long long factorial(int n) {
    if(n == 0) {
        return 1;
    }
    return n * factorial(n - 1);
}

// Fungsi yang dijalankan oleh thread untuk menghitung faktorial dari elemen matriks
void *calculate_factorial(void *arg) {
    int index = (int)arg;
    int row = index / 5;
    int col = index % 5;
    result[row][col] = factorial(value[row][col]);
}

int main() {
    key_t key = 1641;
    int shmid = shmget(key, sizeof(int[4][5]), IPC_CREAT | 0666);
    value = shmat(shmid, NULL, 0);
    
    // Mencetak matriks hasil perkalian
    printf("Matriks Hasil:\n");
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 5; j++) {
            printf("%d ", value[i][j]);
        }
        printf("\n");
    }
    
    pthread_t threads[NUM_THREADS];
    
    // Membuat thread untuk menghitung faktorial dari setiap elemen matriks
    for(int i = 0; i < NUM_THREADS; i++) {
        pthread_create(&threads[i], NULL, calculate_factorial, (void *)i);
    }
    
    // Menunggu semua thread selesai
    for(int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    
    // Mencetak matriks hasil faktorial
    printf("\nMatriks Faktorial:\n");
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 5; j++) {
            printf("%llu ", result[i][j]);
        }
        printf("\n");
    }
    
    // Melepaskan memori bersama
    shmdt(value);
    shmctl(shmid, IPC_RMID, NULL);
    
    return 0;
}
```

## Soal 2 bagian D
Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading.

### Cara Penyelesaian
Program ini dibuat untuk menghitung faktorial dari setiap elemen dalam matriks 4x5. Matriks disimpan dalam memori bersama dan diakses menggunakan fungsi ```shmat```. Program ini mencetak matriks asli dan matriks hasil faktorial ke layar menggunakan fungsi ```print_matrix``` dan ```print_factorial_matrix```. Fungsi ```factorial``` digunakan untuk menghitung faktorial dari setiap elemen. Akhirnya, memori bersama dilepaskan dan dihapus menggunakan fungsi ```shmdt``` dan ```shmctl```.

### Source Code
``` sh
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define ROWS 4
#define COLS 5

// Fungsi untuk menghitung faktorial
unsigned long long factorial(int n) {
    if(n == 0) {
        return 1;
    }
    return n * factorial(n - 1);
}

void print_matrix(int rows, int cols, int matrix[rows][cols]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void print_factorial_matrix(int rows, int cols, int matrix[rows][cols]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%llu ", factorial(matrix[i][j]));
        }
        printf("\n");
    }
}

int main() {
    key_t key = 1641;
    int (*value)[COLS];
    int shmid = shmget(key, sizeof(int[ROWS][COLS]), IPC_CREAT | 0666);
    value = shmat(shmid, NULL, 0);
    
    // Mencetak matriks hasil perkalian
    printf("Matriks Hasil:\n");
    print_matrix(ROWS, COLS, value);
    
    // Mencetak matriks hasil faktorial
    printf("\nMatriks Faktorial:\n");
    print_factorial_matrix(ROWS, COLS, value);
    
    // Melepaskan memori bersama
    shmdt(value);
    shmctl(shmid, IPC_RMID, NULL);
    
    return 0;
}
```

##  perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak
Program cinta.c : faktorial setiap elemen dihitung secara bersamaan menggunakan beberapa thread. Ini berarti program dapat memanfaatkan beberapa inti prosesor untuk melakukan perhitungan lebih cepat.

program sisop.c menghitung faktorial setiap elemen secara berurutan, satu demi satu. Ini berarti program hanya dapat menggunakan satu inti prosesor untuk melakukan perhitungan. 

Sehingga,

Dalam hal kinerja, program cinta.c lebih cepat daripada program sisop.c saat dijalankan pada komputer dengan beberapa inti prosesor. 
Namun, perbedaan kinerja aktual akan bergantung pada berbagai faktor seperti jumlah inti prosesor, kecepatan setiap inti, dan beban kerja setiap thread.

Dalam hal hasil, kedua program harus menghasilkan output yang sama dengan matriks input yang sama. 

### Hasil Output
perintah menyimpan program
![image](http://itspreneur.com/wp-content/uploads/2023/05/Screenshot-46-e1683360133454.png)


Output program kalian.c dengan cinta.c
![image](http://itspreneur.com/wp-content/uploads/2023/05/Screenshot-49-e1683360214751.png)

Output program kalian.c dengan sisop.c
![image](http://itspreneur.com/wp-content/uploads/2023/05/Screenshot-50-e1683360304603.png)


# Soal 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

# Catatan
- Untuk mengerjakan soal ini dapat menggunakan contoh implementasi message queue pada modul.
- Perintah DECRYPT akan melakukan decrypt/decode/konversi dengan metode sebagai berikut:
        1. ROT13
ROT13 atau rotate 13 merupakan metode enkripsi sederhana untuk melakukan enkripsi pada tiap karakter di string dengan menggesernya sebanyak 13 karakter.
Contoh:

        2. Base64
Base64 adalah sistem encoding dari data biner menjadi teks yang menjamin tidak terjadinya modifikasi yang akan merubah datanya selama proses transportasi. Tools Decode/Encode.
        3. Hex
Hexadecimal string merupakan kombinasi bilangan hexadesimal (0-9 dan A-F) yang merepresentasikan suatu string. Tools.



## Soal 3 bagian A
Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

### Cara Penyelesaian

### Source Code
```sh

```

## Soal 3 bagian B
User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut:
└── soal3
	├── playlist.txt
	├── song-playlist.json
	├── stream.c
	└── user.c

### Cara Penyelesaian


### Source Code
```sh

```

## Soal 3 bagian C
Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt
Sample Output:
17 - MK
1-800-273-8255 - Logic
1950 - King Princess
…
Your Love Is My Drug - Kesha
YOUTH - Troye Sivan
ZEZE (feat. Travis Scott & Offset) - Kodak Black
 

### Cara Penyelesaian

### Source Code
```sh

     
```

## Soal 3 bagian D
User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.
PLAY "Stereo Heart" 
    sistem akan menampilkan: 
    USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"
PLAY "BREAK"
    sistem akan menampilkan:
    THERE ARE "N" SONG CONTAINING "BREAK":
    1. THE SCRIPT - BREAKEVEN
    2. ARIANA GRANDE - BREAK FREE
dengan “N” merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":
PLAY "UVUWEVWEVWVE"
    THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"

Untuk mempermudah dan memperpendek kodingan, query bersifat tidak case sensitive 😀


### Cara Penyelesaian

### Source Code
```sh

```


## Soal 3 bagian E
User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:
User mengirimkan perintah
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>

User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”


### Cara Penyelesaian


### Source Code
```sh

```

## Soal 3 bagian F
Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.


### Cara Penyelesaian

### Source Code
```sh

```
## Soal 3 bagian G
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

### Cara Penyelesaian

### Source Code
```sh

```

## output program soal 3
TEXT
![image]()

TEXT
![image]()

# Soal 4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

## Soal 4 bagian A
Download dan unzip file tersebut dalam kode c bernama unzip.c.

### Cara Penyelesaian
- Mengunduh file dari URL tertentu menggunakan perintah wget dan menyimpannya dengan nama file tertentu.
- Mengekstrak file yang diunduh menggunakan perintah unzip.

Kode ini menggunakan fungsi fork() dan execlp() untuk membuat proses anak dan menjalankan perintah wget, unzip, dan rm. Fungsi wait() digunakan untuk menunggu proses anak selesai.

### Source Code
```sh
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>

#define FILE_URL "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download"
#define FILE_NAME "hehe.zip"

void download_file(const char *file_url, const char *file_name) {
    pid_t child_id = fork();
    if (child_id == 0) {
        execlp("wget", "wget", "-O", file_name, file_url, NULL);
    }
    int status;
    while ((wait(&status)) > 0);
}

void unzip_file(const char *file_name) {
    pid_t child_id = fork();
    if (child_id == 0) {
        execlp("unzip", "unzip", file_name, NULL);
    }
    int status;
    while ((wait(&status)) > 0);
}

void remove_file(const char *file_name) {
    pid_t child_id = fork();
    if (child_id == 0) {
        execlp("rm", "rm", file_name, NULL);
    }
    int status;
    while ((wait(&status)) > 0);
}

int main() {
    download_file(FILE_URL, FILE_NAME);
    unzip_file(FILE_NAME);
    remove_file(FILE_NAME);

    printf("Dowload Complete\n");
}

```

## Soal 4 bagian B
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.


### Cara Penyelesaian
dibuat program ```categorize.c``` yang digunakan untuk mengumpulkan file sesuai dengan ekstensinya. Pertama-tama, mendapatkan existensi dari nama file lalu dilakuakn pengecheckan kemudian dibuat folder categorized dan baca file extensions.txt untuk mendapatkan daftar eksitensi di dalamnya buat folder untuk setiap ekstensi dengan nama folder sesuai dengan nama ekstensi (semua huruf kecil).

Selanjutnya, baca file max.txt untuk mendapatkan jumlah maksimum file yang dapat disimpan dalam setiap folder ekstensi (kecuali folder other). Kemudian, dilakuakn pengecekan ekstensi. Jika ekstensinya terdapat dalam daftar ekstensi yang ingin dikumpulkan, pindahkan atau salin file tersebut ke folder yang sesuai dengan ekstensinya. Jika jumlah file dalam folder tersebut sudah mencapai jumlah maksimum yang ditentukan dalam file max.txt, buat folder baru dengan format yang ditentukan. Jika ekstensinya tidak terdapat dalam daftar ekstensi yang ingin dikumpulkan, pindahkan atau salin file tersebut ke folder other.
### Source Code
```sh
// Mendapatkan ekstensi dari nama file
const char *get_filename_ext(char *filename) {
    const char *dot = strrchr(filename, '.');
    if (!dot || dot == filename) return "";
    return dot + 1;
}

// Mengecek ekstensi
int check_ekstension(char eks[]) {
    for (int i = 0; i < extCount; i++) {
        if (strcmp(eks, ekstension[i]) == 0)
            return i;
    }
    return -1;
}
// Membuat direktori
void *buatdir() {
    char folder_name[MAX_LINE_LENGTH];
    strcpy(folder_name, "./categorized");
    char cmd[MAX_LINE_LENGTH];
    sprintf(cmd, "mkdir -p %s", folder_name);
    system(cmd);
    create_thread_logging(3, folder_name, "null", "null");
    FILE *textfile;
    textfile = fopen("extensions.txt", "r");
    int index = 0;
    char line[MAX_LINE_LENGTH];

    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        line[strlen(line) - 2] = '\0';
        sprintf(cmd, "mkdir -p %s/%s", folder_name, line);
        strcpy(ekstension[index], line);
        ekstensionDir[index] = 0;
        index++;
        extCount++;
        system(cmd);

        sprintf(buffer, "%s/%s", folder_name, line);
        create_thread_logging(3, buffer, "null", "null");
        fclose(textfile);

    sprintf(cmd, "mkdir -p %s/other", folder_name);
    system(cmd);

    sprintf(buffer, "%s/other", folder_name);
    create_thread_logging(3, buffer, "null", "null");
}
}

// Mendaftar direktori
void listdir(const char *name) {
    DIR *dir;
    struct dirent *entry;
    char cmd[MAX_LINE_LENGTH];

    // Membuka direktori
    if (!(dir = opendir(name)))
        return;

    // Membaca isi direktori
    while ((entry = readdir(dir)) != NULL) {
        // Jika entry adalah direktori
        if (entry->d_type == DT_DIR) {
            char path[1024];
            // Lewati direktori "." dan ".."
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            create_thread_logging(1, path, "null", "null");
            listdir(path);
        } else {
            // Jika entry adalah file
            sprintf(cmd, "echo '%s/%s' >> listFiles.txt", name, entry->d_name);
            system(cmd);
        }
    }
    closedir(dir);
}

// Mendaftar file
void *listfile() {
    char charMax[3];
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "max.txt");
    textfile = fopen(nama_file, "r");
    fgets(charMax, 3, textfile);
    fclose(textfile);
    max = atoi(charMax);

    listdir("./files");
}

// Memindahkan file
void *pindahfile() {
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "listFiles.txt");
    textfile = fopen(nama_file, "r");

    // Membaca isi file listFiles.txt
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        line[strlen(line) - 1] = '\0';
        char eks[MAX_LINE_LENGTH];
        sprintf(eks,"%s",get_filename_ext(line));

        // Mengubah ekstensi menjadi huruf kecil
        for (int i = 0; eks[i]; i++) {
            eks[i] = tolower(eks[i]);
        }

        // Jika ekstensi tidak ditemukan di daftar ekstensi
        if (check_ekstension(eks) == -1) {
            sprintf(cmd, "cp '%s' ./categorized/other", line);
            system(cmd);
            create_thread_logging(2, "other", line,"./categorized/other" );
        } else {
            // Jika jumlah file dengan ekstensi ini kurang dari max
            if (ekstensionDir[check_ekstension(eks)] < max) {
                sprintf(cmd, "cp '%s' ./categorized/%s", line, eks);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer, "./categorized/%s", eks);
                create_thread_logging(2, eks,line , buffer );
            } else {
                int num = ekstensionDir[check_ekstension(eks)] / max + 1;
                sprintf(buffer, "./categorized/%s %d", eks,num);
                if (access(buffer,F_OK) != 0) {
                    create_thread_logging(3, buffer,"null","null" );
                }
                sprintf(cmd,"mkdir -p './categorized/%s %d'",eks,num);
                system(cmd);

                sprintf(cmd,"cp '%s' './categorized/%s %d'",line ,eks,num);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer,"./categorized/%s %d",eks,num);
                create_thread_logging(2 ,eks,line ,buffer );
            }
        }
        }
    fclose(textfile);
}

```

## Soal 4 bagian C
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file


### Cara Penyelesaian
dibuat sebuah array untuk menyimpan jumlah file untuk setiap ekstensi (termasuk other). Kemudian, untuk setiap file yang dipindahkan atau disalin ke folder yang sesuai dengan ekstensinya, lalu ditambahkan jumlah file untuk ekstensi tersebut dalam array.
### Source Code
```sh


// Menghitung jumlah ekstensi
void *hitung_eks() {
    strcpy(ekstension[extCount], "other");

    DIR *directory;
    struct dirent *entry;

    directory = opendir("./categorized/other");

    create_thread_logging(1,"./categorized/other","null","null");

    while ((entry = readdir(directory)) != NULL) {
        if (entry->d_type == DT_REG) {
            ekstensionDir[extCount]++;
        }
    }
}


```

## Soal 4 bagian D 
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

### Cara Penyelesaian

### Source Code
```sh

```

## Soal 4 bagian E 
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
- Path dimulai dari folder files atau categorized
- Simpan di dalam log.txt
- ACCESSED merupakan folder files beserta dalamnya
- Urutan log tidak harus sama


### Cara Penyelesaian
dibuat folder pada program categorize.c, lalu dibuat fungsi ```loging()``` untuk menulis log ke file log.txt dengan format yang telah ditentukan setelah setiap operasi tersebut. Misalnya, setelah mengakses folder, tulis log akses folder ke file log.txt dengan format DD-MM-YYYY HH:MM:SS ACCESSED [folder path]. Setelah memindahkan file, tulis log pemindahan file ke file log.txt dengan format DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]. Setelah membuat folder, tulis log pembuatan folder ke file log.txt dengan format DD-MM-YYYY HH:MM:SS MADE [folder name]. dengan untuk menjalakan fungsi ```loging()``` dibuat Fungsi ```create_thread_logging(...)``` guna untuk membuat thread yang menjalankan fungsi ```logging(...)```
### Source Code
```sh

// Fungsi untuk logging
void *logging() {
    char file_log[] = "log.txt";
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    char write[MAX_LINE_LENGTH];
    if (mode == 1) {
        sprintf(write, "%s AKSES %s", datetime, param1);
    } else if (mode == 2) {
        sprintf(write, "%s PINDAH %s file : %s > %s", datetime, param1, param2, param3);
    } else {
        sprintf(write, "%s BUAT %s", datetime, param1);
    }
    char cmd[MAX_LINE_LENGTH];
    sprintf(cmd, "echo '%s' >> %s", write, file_log);
    system(cmd);
}

// Membuat thread untuk logging
void create_thread_logging(int modeNew, char param1New[], char param2New[], char param3New[]) {
    mode = modeNew;
    strcpy(param1, param1New);
    strcpy(param2, param2New);
    strcpy(param3, param3New);
    pthread_join(threads[1], NULL);
    pthread_create(&threads[1], NULL, logging, NULL);
    pthread_join(threads[1], NULL);
}

```


## Soal 4 bagian F 
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
Untuk menghitung banyaknya ACCESSED yang dilakukan.
Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.



### Cara Penyelesaian
Dibuat program logchecker.c yang membuka file log.txt dan membaca setiap barisnya. Untuk setiap baris yang dibaca, cek apakah baris tersebut merupakan total folder ACCESSED, list folder yang telah dibuat, atau total file exsptension dan ekstrak informasi yang sesuai. Setelah semua baris telah dibaca, urutkan dan outputkan informasi yang telah diekstrak ke terminal dengan format yang diinginkan.

### Source Code
```sh
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 300

typedef struct {
    char name[MAX_LINE_LENGTH];
    int count;
} Dir;

Dir dir[MAX_LINE_LENGTH];
int indexDir = 0;

typedef struct {
    char name[MAX_LINE_LENGTH];
    int count;
} Eks;

Eks eks[MAX_LINE_LENGTH];
int indexEks = 0;

// Menghitung jumlah akses
int hitung_akses() {
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    int counter = 0;
    textfile = fopen("log.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        if (strstr(line, "ACCESSED") != NULL) {
            counter++;
        }
    }
    fclose(textfile);
    return counter;
}

// Mengecek direktori
int cek_dir(char dirs[]) {
    for (int i = 0; i < indexDir; i++) {
        if (strcmp(dirs, dir[i].name) == 0)
            return i;
    }
    return -1;
}

// Mengecek ekstensi
int cek_eks(char ekss[]) {
    for (int i = 0; i < indexEks; i++) {
        if (strcmp(ekss, eks[i].name) == 0)
            return i;
    }
    return -1;
}

// Menghitung semua direktori
void hitung_semua_dir() {
    FILE *textfile;
    char line[MAX_LINE_LENGTH];

    textfile = fopen("log.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        if (strstr(line, "MADE ") != NULL) {
            line[strlen(line) - 1] = '\0';
            char *pos = strstr(line, "MADE ");
            char *file = pos + strlen("MADE ");

            strcpy(dir[indexDir].name, file);
            dir[indexDir].count = 0;
            indexDir++;
        }
    }
    fclose(textfile);

    textfile = fopen("log.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        if (strstr(line, "> ") != NULL) {
            line[strlen(line) - 1] = '\0';
            char *pos = strstr(line, "> ");
            char *file = pos + strlen("> ");

            int num = cek_dir(file);
            dir[num].count++;
        }
    }
    fclose(textfile);

    for (int i = 0; i < indexDir; i++) {
        for (int j = 0; j < indexDir - 1; j++) {
            if (dir[j].count > dir[j + 1].count) {
                Dir temp;
                temp = dir[j];
                dir[j] = dir[j + 1];
                dir[j + 1] = temp;
            }
        }
    }

    for (int i = 0; i < indexDir; i++) {
        printf("%s : %d\n", dir[i].name, dir[i].count);
    }
}

// Menghitung semua ekstensi
void hitung_semua_eks() {
    FILE *textfile;
    char line[MAX_LINE_LENGTH];

    textfile = fopen("extensions.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        line[strlen(line) - 2] = '\0';
        strcpy(eks[indexEks].name, line);
        eks[indexEks].count = 0;
        indexEks++;
    }
    fclose(textfile);
    strcpy(eks[indexEks].name, "other");
    eks[indexEks].count = 0;
    indexEks++;
textfile = fopen("log.txt", "r");
    while (fgets(line, MAX_LINE_LENGTH, textfile)) {
        if (strstr(line, "MOVED ") != NULL) {
            line[strlen(line) - 1] = '\0';
            char *start = strstr(line, "MOVED ");
            char *en = strstr(start, " file :");
            char file[MAX_LINE_LENGTH];
            strncpy(file, start + strlen("MOVED "), en - start - strlen("MOVED "));
            file[en - start - strlen("MOVED ")] = '\0';

            int num = cek_eks(file);
            eks[num].count++;
        }
    }
    fclose(textfile);

    for (int i = 0; i < indexEks; i++) {
        for (int j = 0; j < indexEks - 1; j++) {
            if (eks[j].count > eks[j + 1].count) {
                Eks temp;
                temp = eks[j];
                eks[j] = eks[j + 1];
                eks[j + 1] = temp;
            }
        }
    }

    for (int i = 0; i < indexEks; i++) {
        printf("%s : %d\n", eks[i].name, eks[i].count);
    }
}

int main() {

    printf(" list seluruh folder yang telah dibuat : \n");
    hitung_semua_dir();

    printf("\n banyaknya total file tiap extension, terurut secara ascending : \n");
    hitung_semua_eks();

    printf(" total ACCESSED : %d\n\n", hitung_akses());
}

```

## output program soal 4
TEXT
![image](http://itspreneur.com/wp-content/uploads/2023/05/Screenshot-69-3-e1683910676908.png)

TEXT
![image](http://itspreneur.com/wp-content/uploads/2023/05/Screenshot-68-1-e1683910726614.png)

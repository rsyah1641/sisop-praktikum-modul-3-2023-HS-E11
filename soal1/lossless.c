#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>

#define size_bufer 1024

typedef struct save_hufmand {
    unsigned char no;
    unsigned int dataa;
    int lost;
} save_hufmand;

typedef struct node_simpul {
    unsigned char no;
    int lim;
    struct node_simpul *left_child, *right_child;
} node_simpul;

int lim[256] = {0};

// Menghitung frekuensi karakter dalam file
void hitung_frekuensi(char *filename)
{
    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        perror("File Cannot Open");
        exit(EXIT_FAILURE);
    }

    int no;
    while ((no = fgetc(file)) != EOF) {
        lim[no]++;
    }

    fclose(file);
}

// Membangun pohon Huffman dari array frekuensi
node_simpul *bangun_pohon_huffman(int *lim)
{
    node_simpul *nodes[256] = {NULL};
    int n_nodes = 0;

    for (int i = 0; i < 256; i++) {
        if (lim[i] > 0) {
            node_simpul *node = (node_simpul *) malloc(sizeof(node_simpul));
            node->no = (unsigned char)i;
            node->lim = lim[i];
            node->left_child = NULL;
            node->right_child = NULL;

            nodes[n_nodes++] = node;
        }
    }

    while (n_nodes > 1) {
        // Find two nodes with lowest frequency
        int min1 = 0, min2 = 1;
        if (nodes[min1]->lim > nodes[min2]->lim) {
            int tmp = min1;
            min1 = min2;
            min2 = tmp;
        }

        for (int i = 2; i < n_nodes; i++) {
            if (nodes[i]->lim < nodes[min1]->lim) {
                min2 = min1;
                min1 = i;
            } else if (nodes[i]->lim < nodes[min2]->lim) {
                min2 = i;
            }
        }

        // Create new node and add to list
        node_simpul *new_node = (node_simpul *) malloc(sizeof(node_simpul));
        new_node->no = '\0';
        new_node->lim = nodes[min1]->lim + nodes[min2]->lim;
        new_node->left_child = nodes[min1];
        new_node->right_child = nodes[min2];

        nodes[min1] = new_node;
        nodes[min2] = nodes[--n_nodes];
    }

    return nodes[0];
}

// Membangun tabel kode dari pohon Huffman
void bangun_tabel_kode(node_simpul *node, unsigned int dataa, int lost, save_hufmand **table, int *size)
{
    if (node->left_child == NULL && node->right_child == NULL) {
        // Found a leaf node, add dataa to table
        (*size)++;
        (*table) = (save_hufmand *) realloc(*table, sizeof(save_hufmand) * (*size));
        (*table)[*size - 1].no = node->no;
        (*table)[*size - 1].dataa = dataa;
        (*table)[*size - 1].lost = lost;
    } else {
        bangun_tabel_kode(node->left_child, dataa << 1, lost + 1, table, size);
        bangun_tabel_kode(node->right_child, (dataa << 1) | 1, lost + 1, table, size);
    }
}

// Menulis pohon Huffman ke file
void tulis_pohon_huffman(node_simpul *node, int output_fd)
{
    if (node->left_child == NULL && node->right_child == NULL) {
        // Found a leaf node, write '1' bit followed by 8-bit character dataa
        unsigned char byte = (1 << 7) | node->no;
        write(output_fd, &byte, 1);
    } else {
        // Write '0' bit and recursively write subtrees
        unsigned char byte = 0;
        write(output_fd, &byte, 1);

        tulis_pohon_huffman(node->left_child, output_fd);
        tulis_pohon_huffman(node->right_child, output_fd);
    }
}
// Membaca pohon Huffman dari file
node_simpul *baca_pohon_huffman(int input_fd)
{
    unsigned char byte;
    ssize_t nread = read(input_fd, &byte, 1);

    if (nread == 0 || (nread == 1 && byte == 0)) {
        // Empty tree or end of subtree, return NULL
        return NULL;
    }

    if (byte & (1 << 7)) {
        // Leaf node, read 
        unsigned char dataa = byte & ~(1 << 7);
        node_simpul *node = (node_simpul *) malloc(sizeof(node_simpul));
        node->no = dataa;
        node->lim = lim[dataa];
        node->left_child = NULL;
        node->right_child = NULL;

        return node;
    } else {
        // Internal node, recursively read subtrees
        node_simpul *node = (node_simpul *) malloc(sizeof(node_simpul));
        node->no = '\0';
        node->lim = 0;
        node->left_child = baca_pohon_huffman(input_fd);
        node->right_child = baca_pohon_huffman(input_fd);

        return node;
    }
}

// Fungsi untuk kompresi file
void kompres(char *input_filename, char *output_filename)
{
    hitung_frekuensi(input_filename);
    node_simpul *root = bangun_pohon_huffman(lim);

    save_hufmand *table = NULL;
    int size = 0;
    bangun_tabel_kode(root, 0, 0, &table, &size);

    int output_fd = open(output_filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    if (output_fd == -1) {
        perror("Error opening output file");
        exit(EXIT_FAILURE);
    }

    tulis_pohon_huffman(root, output_fd);

    unsigned char buffer[size_bufer];
    unsigned int bit_buffer = 0;
    int num_bits = 0;

    FILE *input_file = fopen(input_filename, "r");

    if (input_file == NULL) {
        perror("Error opening input file");
        exit(EXIT_FAILURE);
    }

    int no;
    while ((no = fgetc(input_file)) != EOF) {
        for (int i = 0; i < size; i++) {
            if (table[i].no == no) {
                bit_buffer |= table[i].dataa << num_bits;
                num_bits += table[i].lost;

                while (num_bits >= 8) {
                    buffer[0] = bit_buffer & 0xff;
                    write(output_fd, buffer, 1);

                    bit_buffer >>= 8;
                    num_bits -= 8;
                }
            }
              }
    }

    // Flush remaining bits to output
    if (num_bits > 0) {
        buffer[0] = bit_buffer & 0xff;
        write(output_fd, buffer, 1);
    }

    fclose(input_file);
    close(output_fd);

    // Calculate compression ratio
    struct stat input_stat, output_stat;

    if (stat(input_filename, &input_stat) == -1 || stat(output_filename, &output_stat) == -1) {
        perror("Error getting file information");
        exit(EXIT_FAILURE);
    }

    printf("Original size: %lld bytes\n", (long long) input_stat.st_size);
    printf("Compressed size: %lld bytes\n", (long long) output_stat.st_size);
}

// Fungsi untuk dekompresi file
void dekompres(char *input_filename, char *output_filename)
{
    int input_fd = open(input_filename, O_RDONLY);

    if (input_fd == -1) {
        perror("Error opening input file");
        exit(EXIT_FAILURE);
    }

    node_simpul *root = baca_pohon_huffman(input_fd);

    FILE *output_file = fopen(output_filename, "w");

    if (output_file == NULL) {
        perror("Error opening output file");
        exit(EXIT_FAILURE);
    }

    node_simpul *node = root;
    unsigned char buffer[size_bufer];
    int buffer_pos = size_bufer;

    while (1) {
        if (buffer_pos == size_bufer) {
            ssize_t nread = read(input_fd, buffer, size_bufer);

            if (nread == 0) {
                break;  // End of input file
            }

            buffer_pos = 0;
        }

        unsigned char byte = buffer[buffer_pos++];

        for (int i = 0; i < 8; i++) {
            if (node->left_child == NULL && node->right_child == NULL) {
                // Found a leaf node, write character to output file
                fputc(node->no, output_file);
                node = root;
            }

            if (byte & (1 << (7 - i))) {
                node = node->right_child;
            } else {
                node = node->left_child;
            }
        }
          }

    fclose(output_file);
    close(input_fd);
}


int main(int argc, char *argv[])
{
    if (argc != 4) {
        fprintf(stderr, "Usage: %s [kompresi| dekompresi] input_filename output_filename\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        // Child process
        if (strcmp(argv[1], "kompresi") == 0) {
            kompres(argv[2], argv[3]);
        } 
    } else {
        // Parent process
        if (strcmp(argv[1], "dekompresi") == 0) {
            dekompres(argv[2], argv[3]);
        }
        int status;
        wait(&status);
        if (WIFEXITED(status)) {
            printf("Child process exited%d\n", WEXITSTATUS(status));
        }
    }
    return 0;
}
